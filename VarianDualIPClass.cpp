/*----- PROTECTED REGION ID(VarianDualIPClass.cpp) ENABLED START -----*/
static const char *RcsId      = "$Id: VarianDualIPClass.cpp,v 1.10 2012/05/21 08:54:09 pascal_verdier Exp $";
static const char *TagName    = "$Name:  $";
static const char *CvsPath    = "$Source: /cvsroot/tango-ds/Vacuum/VarianDualIP/VarianDualIPClass.cpp,v $";
static const char *SvnPath    = "$HeadURL:  $";
static const char *HttpServer = "http://www.esrf.eu/computing/cs/tango/tango_doc/ds_doc/";
//=============================================================================
//
// file :        VarianDualIPClass.cpp
//
// description : C++ source for the VarianDualIPClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the �name� once per process.
//
// project :     Varian Dual Ion Pump.
//
// $Author: pascal_verdier $
//
// $Revision: 1.10 $
// $Date: 2012/05/21 08:54:09 $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source: /cvsroot/tango-ds/Vacuum/VarianDualIP/VarianDualIPClass.cpp,v $
// $Log: VarianDualIPClass.cpp,v $
// Revision 1.10  2012/05/21 08:54:09  pascal_verdier
// Change Voltage format.
// Fix a bug on Current/Voltage.
//
// Revision 1.9  2011/05/31 06:05:43  pascal_verdier
// Cosmetics changes
//
// Revision 1.8  2010/10/11 06:55:58  pascal_verdier
// Pogo-7 compatibility
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <tango.h>
#include <VarianDualIP.h>
#include <VarianDualIPClass.h>

/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass.cpp

//-------------------------------------------------------------------
/**
 *	Create VarianDualIPClass singleton and
 *	return it in a C function for Python usage
 */
//-------------------------------------------------------------------
extern "C" {
#ifdef _TG_WINDOWS_

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_VarianDualIP_class(const char *name) {
		return VarianDualIP_ns::VarianDualIPClass::init(name);
	}
}

namespace VarianDualIP_ns
{
//===================================================================
//	Initialize pointer for singleton pattern
//===================================================================
VarianDualIPClass *VarianDualIPClass::_instance = NULL;

//--------------------------------------------------------
/**
 * method : 		VarianDualIPClass::VarianDualIPClass(string &s)
 * description : 	constructor for the VarianDualIPClass
 *
 * @param s	The class name
 */
//--------------------------------------------------------
VarianDualIPClass::VarianDualIPClass(string &s):Tango::DeviceClass(s)
{
	cout2 << "Entering VarianDualIPClass constructor" << endl;
	set_default_property();
	write_class_property();

	/*----- PROTECTED REGION ID(VarianDualIPClass::constructor) ENABLED START -----*/

	

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::constructor

	cout2 << "Leaving VarianDualIPClass constructor" << endl;
}

//--------------------------------------------------------
/**
 * method : 		VarianDualIPClass::~VarianDualIPClass()
 * description : 	destructor for the VarianDualIPClass
 */
//--------------------------------------------------------
VarianDualIPClass::~VarianDualIPClass()
{
	/*----- PROTECTED REGION ID(VarianDualIPClass::destructor) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::destructor

	_instance = NULL;
}


//--------------------------------------------------------
/**
 * method : 		VarianDualIPClass::init
 * description : 	Create the object if not already done.
 *                  Otherwise, just return a pointer to the object
 *
 * @param	name	The class name
 */
//--------------------------------------------------------
VarianDualIPClass *VarianDualIPClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new VarianDualIPClass(s);
		}
		catch (bad_alloc &)
		{
			throw;
		}
	}
	return _instance;
}

//--------------------------------------------------------
/**
 * method : 		VarianDualIPClass::instance
 * description : 	Check if object already created,
 *                  and return a pointer to the object
 */
//--------------------------------------------------------
VarianDualIPClass *VarianDualIPClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}



//===================================================================
//	Command execution method calls
//===================================================================
//--------------------------------------------------------
/**
 * method : 		OnClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OnClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "OnClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->on());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		OffClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OffClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "OffClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->off());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		ResetClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *ResetClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "ResetClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->reset());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		SetFixedModeClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SetFixedModeClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "SetFixedModeClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->set_fixed_mode());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		SetStartModeClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SetStartModeClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "SetStartModeClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->set_start_mode());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		SetStepModeClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SetStepModeClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "SetStepModeClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->set_step_mode());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		SetProtectModeClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SetProtectModeClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "SetProtectModeClass::execute(): arrived" << endl;
	((static_cast<VarianDualIP *>(device))->set_protect_mode());
	return new CORBA::Any();
}


//===================================================================
//	Properties management
//===================================================================
//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::get_class_property()
 *	Description : Get the class property for specified name.
 */
//--------------------------------------------------------
Tango::DbDatum VarianDualIPClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, returns  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::get_default_device_property()
 *	Description : Return the default value for device property.
 */
//--------------------------------------------------------
Tango::DbDatum VarianDualIPClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::get_default_class_property()
 *	Description : Return the default value for class property.
 */
//--------------------------------------------------------
Tango::DbDatum VarianDualIPClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}


//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::set_default_property()
 *	Description : Set default property (class and device) for wizard.
 *                For each property, add to wizard property name and description.
 *                If default value has been set, add it to wizard property and
 *                store it in a DbDatum.
 */
//--------------------------------------------------------
void VarianDualIPClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;
	vector<string>	vect_data;

	//	Set Default Class Properties

	//	Set Default device Properties
	prop_name = "ChannelNumber";
	prop_desc = "The channel number of the ion pump (1 or 2)";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "ControllerDeviceName";
	prop_desc = "The device name of the associated ion pump controller";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
}

//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::write_class_property()
 *	Description : Set class description fields as property in database
 */
//--------------------------------------------------------
void VarianDualIPClass::write_class_property()
{
	//	First time, check if database used
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Varian Dual Ion Pump");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("This device server is intended  to drive and monitor a ion pump controlled by a");
	str_desc.push_back("Varian Dual ion pump controller. You need VarianDualCrtl device classes OR Agilent4uhv device classes");
	description << str_desc;
	data.push_back(description);

	//	put cvs or svn location
	string	filename("VarianDualIP");
	filename += "Class.cpp";

	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}

	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("TANGO_BASE_CLASS");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	get_db_class()->put_property(data);
}

//===================================================================
//	Factory methods
//===================================================================

//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::device_factory()
 *	Description : Create the device object(s)
 *                and store them in the device list
 */
//--------------------------------------------------------
void VarianDualIPClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{
	/*----- PROTECTED REGION ID(VarianDualIPClass::device_factory_before) ENABLED START -----*/

	

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::device_factory_before

	//	Create devices and add it into the device list
	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
		device_list.push_back(new VarianDualIP(this, (*devlist_ptr)[i]));
	}

	//	Manage dynamic attributes if any
	erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

	//	Export devices to the outside world
	for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
	{
		//	Add dynamic attributes if any
		VarianDualIP *dev = static_cast<VarianDualIP *>(device_list[device_list.size()-i]);
		dev->add_dynamic_attributes();

		//	Check before if database used.
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(dev);
		else
			export_device(dev, dev->get_name().c_str());
	}

	/*----- PROTECTED REGION ID(VarianDualIPClass::device_factory_after) ENABLED START -----*/

	

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::device_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::attribute_factory()
 *	Description : Create the attribute object(s)
 *                and store them in the attribute list
 */
//--------------------------------------------------------
void VarianDualIPClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	/*----- PROTECTED REGION ID(VarianDualIPClass::attribute_factory_before) ENABLED START -----*/

	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::attribute_factory_before
	//	Attribute : Pressure
	PressureAttrib	*pressure = new PressureAttrib();
	Tango::UserDefaultAttrProp	pressure_prop;
	//	description	not set for Pressure
	pressure_prop.set_label("Pressure");
	pressure_prop.set_unit("mBar");
	pressure_prop.set_standard_unit("mBar");
	pressure_prop.set_display_unit("mBar");
	pressure_prop.set_format("%1.1e");
	//	max_value	not set for Pressure
	//	min_value	not set for Pressure
	//	max_alarm	not set for Pressure
	//	min_alarm	not set for Pressure
	//	max_warning	not set for Pressure
	//	min_warning	not set for Pressure
	//	delta_t	not set for Pressure
	//	delta_val	not set for Pressure
	
	pressure->set_default_properties(pressure_prop);
	//	Not Polled
	pressure->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(pressure);

	//	Attribute : Voltage
	VoltageAttrib	*voltage = new VoltageAttrib();
	Tango::UserDefaultAttrProp	voltage_prop;
	//	description	not set for Voltage
	//	label	not set for Voltage
	voltage_prop.set_unit("V");
	//	standard_unit	not set for Voltage
	//	display_unit	not set for Voltage
	voltage_prop.set_format("%4.0f");
	//	max_value	not set for Voltage
	//	min_value	not set for Voltage
	//	max_alarm	not set for Voltage
	voltage_prop.set_min_alarm("1500");
	//	max_warning	not set for Voltage
	//	min_warning	not set for Voltage
	//	delta_t	not set for Voltage
	//	delta_val	not set for Voltage
	
	voltage->set_default_properties(voltage_prop);
	//	Not Polled
	voltage->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(voltage);

	//	Attribute : Current
	CurrentAttrib	*current = new CurrentAttrib();
	Tango::UserDefaultAttrProp	current_prop;
	//	description	not set for Current
	current_prop.set_label("Current");
	current_prop.set_unit("A");
	current_prop.set_standard_unit("A");
	current_prop.set_display_unit("A");
	current_prop.set_format("%1.1e");
	//	max_value	not set for Current
	//	min_value	not set for Current
	//	max_alarm	not set for Current
	//	min_alarm	not set for Current
	//	max_warning	not set for Current
	//	min_warning	not set for Current
	//	delta_t	not set for Current
	//	delta_val	not set for Current
	
	current->set_default_properties(current_prop);
	//	Not Polled
	current->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(current);

	//	Attribute : Size
	SizeAttrib	*size = new SizeAttrib();
	Tango::UserDefaultAttrProp	size_prop;
	size_prop.set_description("change size of the pump is avaible only for 4uhv Controler.\nAcceptable size :\nSpare, 500 SC/Tr,300 SC/Tr,150 SC/Tr,75-55-40 SC/Tr,\n20 SC/Tr, 500 Diode/ND, 300 Diode/ND, 150 Diode/ND,\n75-55-40 Diode/ND, 20-25 Diode/ND, 10 Diode/ND,\n75-55-45 SEM,20-35 SE,10 SEM");
	//	label	not set for Size
	//	unit	not set for Size
	//	standard_unit	not set for Size
	//	display_unit	not set for Size
	//	format	not set for Size
	//	max_value	not set for Size
	//	min_value	not set for Size
	//	max_alarm	not set for Size
	//	min_alarm	not set for Size
	//	max_warning	not set for Size
	//	min_warning	not set for Size
	//	delta_t	not set for Size
	//	delta_val	not set for Size
	
	size->set_default_properties(size_prop);
	//	Not Polled
	size->set_disp_level(Tango::OPERATOR);
	size->set_memorized();
	size->set_memorized_init(false);
	att_list.push_back(size);

	//	Attribute : ControlType
	ControlTypeAttrib	*controltype = new ControlTypeAttrib();
	Tango::UserDefaultAttrProp	controltype_prop;
	//	description	not set for ControlType
	//	label	not set for ControlType
	//	unit	not set for ControlType
	//	standard_unit	not set for ControlType
	//	display_unit	not set for ControlType
	//	format	not set for ControlType
	//	max_value	not set for ControlType
	//	min_value	not set for ControlType
	//	max_alarm	not set for ControlType
	//	min_alarm	not set for ControlType
	//	max_warning	not set for ControlType
	//	min_warning	not set for ControlType
	//	delta_t	not set for ControlType
	//	delta_val	not set for ControlType
	
	controltype->set_default_properties(controltype_prop);
	//	Not Polled
	controltype->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(controltype);

	//	Attribute : FirstStepCurrent
	FirstStepCurrentAttrib	*firststepcurrent = new FirstStepCurrentAttrib();
	Tango::UserDefaultAttrProp	firststepcurrent_prop;
	//	description	not set for FirstStepCurrent
	firststepcurrent_prop.set_label("First step current");
	firststepcurrent_prop.set_unit("A");
	firststepcurrent_prop.set_standard_unit("A");
	firststepcurrent_prop.set_display_unit("A");
	firststepcurrent_prop.set_format("%3.2e");
	//	max_value	not set for FirstStepCurrent
	//	min_value	not set for FirstStepCurrent
	//	max_alarm	not set for FirstStepCurrent
	//	min_alarm	not set for FirstStepCurrent
	//	max_warning	not set for FirstStepCurrent
	//	min_warning	not set for FirstStepCurrent
	//	delta_t	not set for FirstStepCurrent
	//	delta_val	not set for FirstStepCurrent
	
	firststepcurrent->set_default_properties(firststepcurrent_prop);
	//	Not Polled
	firststepcurrent->set_disp_level(Tango::OPERATOR);
	firststepcurrent->set_memorized();
	firststepcurrent->set_memorized_init(true);
	att_list.push_back(firststepcurrent);

	//	Attribute : SecondStepCurrent
	SecondStepCurrentAttrib	*secondstepcurrent = new SecondStepCurrentAttrib();
	Tango::UserDefaultAttrProp	secondstepcurrent_prop;
	//	description	not set for SecondStepCurrent
	secondstepcurrent_prop.set_label("Second step current");
	secondstepcurrent_prop.set_unit("A");
	secondstepcurrent_prop.set_standard_unit("A");
	secondstepcurrent_prop.set_display_unit("A");
	secondstepcurrent_prop.set_format("%3.2e");
	//	max_value	not set for SecondStepCurrent
	//	min_value	not set for SecondStepCurrent
	//	max_alarm	not set for SecondStepCurrent
	//	min_alarm	not set for SecondStepCurrent
	//	max_warning	not set for SecondStepCurrent
	//	min_warning	not set for SecondStepCurrent
	//	delta_t	not set for SecondStepCurrent
	//	delta_val	not set for SecondStepCurrent
	
	secondstepcurrent->set_default_properties(secondstepcurrent_prop);
	//	Not Polled
	secondstepcurrent->set_disp_level(Tango::OPERATOR);
	secondstepcurrent->set_memorized();
	secondstepcurrent->set_memorized_init(true);
	att_list.push_back(secondstepcurrent);

	//	Attribute : FixMode
	FixModeAttrib	*fixmode = new FixModeAttrib();
	Tango::UserDefaultAttrProp	fixmode_prop;
	//	description	not set for FixMode
	//	label	not set for FixMode
	//	unit	not set for FixMode
	//	standard_unit	not set for FixMode
	//	display_unit	not set for FixMode
	//	format	not set for FixMode
	//	max_value	not set for FixMode
	//	min_value	not set for FixMode
	//	max_alarm	not set for FixMode
	//	min_alarm	not set for FixMode
	//	max_warning	not set for FixMode
	//	min_warning	not set for FixMode
	//	delta_t	not set for FixMode
	//	delta_val	not set for FixMode
	
	fixmode->set_default_properties(fixmode_prop);
	//	Not Polled
	fixmode->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(fixmode);

	//	Attribute : StartMode
	StartModeAttrib	*startmode = new StartModeAttrib();
	Tango::UserDefaultAttrProp	startmode_prop;
	//	description	not set for StartMode
	//	label	not set for StartMode
	//	unit	not set for StartMode
	//	standard_unit	not set for StartMode
	//	display_unit	not set for StartMode
	//	format	not set for StartMode
	//	max_value	not set for StartMode
	//	min_value	not set for StartMode
	//	max_alarm	not set for StartMode
	//	min_alarm	not set for StartMode
	//	max_warning	not set for StartMode
	//	min_warning	not set for StartMode
	//	delta_t	not set for StartMode
	//	delta_val	not set for StartMode
	
	startmode->set_default_properties(startmode_prop);
	//	Not Polled
	startmode->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(startmode);

	//	Attribute : Controller
	ControllerAttrib	*controller = new ControllerAttrib();
	Tango::UserDefaultAttrProp	controller_prop;
	//	description	not set for Controller
	//	label	not set for Controller
	//	unit	not set for Controller
	//	standard_unit	not set for Controller
	//	display_unit	not set for Controller
	//	format	not set for Controller
	//	max_value	not set for Controller
	//	min_value	not set for Controller
	//	max_alarm	not set for Controller
	//	min_alarm	not set for Controller
	//	max_warning	not set for Controller
	//	min_warning	not set for Controller
	//	delta_t	not set for Controller
	//	delta_val	not set for Controller
	
	controller->set_default_properties(controller_prop);
	//	Not Polled
	controller->set_disp_level(Tango::EXPERT);
	//	Not Memorized
	att_list.push_back(controller);


	//	Create a list of static attributes
	create_static_attribute_list(get_class_attr()->get_attr_list());
	/*----- PROTECTED REGION ID(VarianDualIPClass::attribute_factory_after) ENABLED START -----*/

	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::attribute_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::pipe_factory()
 *	Description : Create the pipe object(s)
 *                and store them in the pipe list
 */
//--------------------------------------------------------
void VarianDualIPClass::pipe_factory()
{
	/*----- PROTECTED REGION ID(VarianDualIPClass::pipe_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::pipe_factory_before
	/*----- PROTECTED REGION ID(VarianDualIPClass::pipe_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::pipe_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::command_factory()
 *	Description : Create the command object(s)
 *                and store them in the command list
 */
//--------------------------------------------------------
void VarianDualIPClass::command_factory()
{
	/*----- PROTECTED REGION ID(VarianDualIPClass::command_factory_before) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::command_factory_before

	//	Set polling perod for command State
	Tango::Command	&stateCmd = get_cmd_by_name("State");
	stateCmd.set_polling_period(3000);
	

	//	Command On
	OnClass	*pOnCmd =
		new OnClass("On",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pOnCmd);

	//	Command Off
	OffClass	*pOffCmd =
		new OffClass("Off",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pOffCmd);

	//	Command Reset
	ResetClass	*pResetCmd =
		new ResetClass("Reset",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pResetCmd);

	//	Command SetFixedMode
	SetFixedModeClass	*pSetFixedModeCmd =
		new SetFixedModeClass("SetFixedMode",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSetFixedModeCmd);

	//	Command SetStartMode
	SetStartModeClass	*pSetStartModeCmd =
		new SetStartModeClass("SetStartMode",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSetStartModeCmd);

	//	Command SetStepMode
	SetStepModeClass	*pSetStepModeCmd =
		new SetStepModeClass("SetStepMode",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSetStepModeCmd);

	//	Command SetProtectMode
	SetProtectModeClass	*pSetProtectModeCmd =
		new SetProtectModeClass("SetProtectMode",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSetProtectModeCmd);

	/*----- PROTECTED REGION ID(VarianDualIPClass::command_factory_after) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::command_factory_after
}

//===================================================================
//	Dynamic attributes related methods
//===================================================================

//--------------------------------------------------------
/**
 * method : 		VarianDualIPClass::create_static_attribute_list
 * description : 	Create the a list of static attributes
 *
 * @param	att_list	the ceated attribute list
 */
//--------------------------------------------------------
void VarianDualIPClass::create_static_attribute_list(vector<Tango::Attr *> &att_list)
{
	for (unsigned long i=0 ; i<att_list.size() ; i++)
	{
		string att_name(att_list[i]->get_name());
		transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
		defaultAttList.push_back(att_name);
	}

	cout2 << defaultAttList.size() << " attributes in default list" << endl;

	/*----- PROTECTED REGION ID(VarianDualIPClass::create_static_att_list) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::create_static_att_list
}


//--------------------------------------------------------
/**
 * method : 		VarianDualIPClass::erase_dynamic_attributes
 * description : 	delete the dynamic attributes if any.
 *
 * @param	devlist_ptr	the device list pointer
 * @param	list of all attributes
 */
//--------------------------------------------------------
void VarianDualIPClass::erase_dynamic_attributes(const Tango::DevVarStringArray *devlist_ptr, vector<Tango::Attr *> &att_list)
{
	Tango::Util *tg = Tango::Util::instance();

	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		Tango::DeviceImpl *dev_impl = tg->get_device_by_name(((string)(*devlist_ptr)[i]).c_str());
		VarianDualIP *dev = static_cast<VarianDualIP *> (dev_impl);

		vector<Tango::Attribute *> &dev_att_list = dev->get_device_attr()->get_attribute_list();
		vector<Tango::Attribute *>::iterator ite_att;
		for (ite_att=dev_att_list.begin() ; ite_att != dev_att_list.end() ; ++ite_att)
		{
			string att_name((*ite_att)->get_name_lower());
			if ((att_name == "state") || (att_name == "status"))
				continue;
			vector<string>::iterator ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
			if (ite_str == defaultAttList.end())
			{
				cout2 << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i] << endl;
				Tango::Attribute &att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
				dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
				--ite_att;
			}
		}
	}
	/*----- PROTECTED REGION ID(VarianDualIPClass::erase_dynamic_attributes) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::erase_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : VarianDualIPClass::get_attr_by_name()
 *	Description : returns Tango::Attr * object found by name
 */
//--------------------------------------------------------
Tango::Attr *VarianDualIPClass::get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname)
{
	vector<Tango::Attr *>::iterator it;
	for (it=att_list.begin() ; it<att_list.end() ; ++it)
		if ((*it)->get_name()==attname)
			return (*it);
	//	Attr does not exist
	return NULL;
}


/*----- PROTECTED REGION ID(VarianDualIPClass::Additional Methods) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::Additional Methods
} //	namespace
