/*----- PROTECTED REGION ID(VarianDualIPClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        VarianDualIPClass.h
//
// description : Include for the VarianDualIPClass root class.
//               This class is the singleton class for.
//               the VarianDualIP device class..
//               It contains all properties and methods which the .
//               VarianDualIP requires only once e.g. the commands.
//
// project :     Varian Dual Ion Pump.
//
// $Author: pascal_verdier $
//
// $Revision: 1.6 $
// $Date: 2012/05/21 08:54:09 $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source: /cvsroot/tango-ds/Vacuum/VarianDualIP/VarianDualIPClass.h,v $
// $Log: VarianDualIPClass.h,v $
// Revision 1.6  2012/05/21 08:54:09  pascal_verdier
// Change Voltage format.
// Fix a bug on Current/Voltage.
//
// Revision 1.5  2011/05/31 06:05:43  pascal_verdier
// Cosmetics changes
//
// Revision 1.4  2010/10/11 06:55:58  pascal_verdier
// Pogo-7 compatibility
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef VARIANDUALIPCLASS_H
#define VARIANDUALIPCLASS_H

#include <tango.h>
#include <VarianDualIP.h>

/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass.h


namespace VarianDualIP_ns
{
/*----- PROTECTED REGION ID(VarianDualIPClass::classes for dynamic creation) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute Pressure class definition
class PressureAttrib: public Tango::Attr
{
public:
	PressureAttrib():Attr("Pressure",
			Tango::DEV_FLOAT, Tango::READ) {};
	~PressureAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_Pressure(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_Pressure_allowed(ty);}
};

//	Attribute Voltage class definition
class VoltageAttrib: public Tango::Attr
{
public:
	VoltageAttrib():Attr("Voltage",
			Tango::DEV_FLOAT, Tango::READ) {};
	~VoltageAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_Voltage(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_Voltage_allowed(ty);}
};

//	Attribute Current class definition
class CurrentAttrib: public Tango::Attr
{
public:
	CurrentAttrib():Attr("Current",
			Tango::DEV_FLOAT, Tango::READ) {};
	~CurrentAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_Current(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_Current_allowed(ty);}
};

//	Attribute Size class definition
class SizeAttrib: public Tango::Attr
{
public:
	SizeAttrib():Attr("Size",
			Tango::DEV_STRING, Tango::READ_WRITE) {};
	~SizeAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_Size(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<VarianDualIP *>(dev))->write_Size(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_Size_allowed(ty);}
};

//	Attribute ControlType class definition
class ControlTypeAttrib: public Tango::Attr
{
public:
	ControlTypeAttrib():Attr("ControlType",
			Tango::DEV_STRING, Tango::READ) {};
	~ControlTypeAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_ControlType(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_ControlType_allowed(ty);}
};

//	Attribute FirstStepCurrent class definition
class FirstStepCurrentAttrib: public Tango::Attr
{
public:
	FirstStepCurrentAttrib():Attr("FirstStepCurrent",
			Tango::DEV_FLOAT, Tango::READ_WRITE) {};
	~FirstStepCurrentAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_FirstStepCurrent(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<VarianDualIP *>(dev))->write_FirstStepCurrent(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_FirstStepCurrent_allowed(ty);}
};

//	Attribute SecondStepCurrent class definition
class SecondStepCurrentAttrib: public Tango::Attr
{
public:
	SecondStepCurrentAttrib():Attr("SecondStepCurrent",
			Tango::DEV_FLOAT, Tango::READ_WRITE) {};
	~SecondStepCurrentAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_SecondStepCurrent(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<VarianDualIP *>(dev))->write_SecondStepCurrent(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_SecondStepCurrent_allowed(ty);}
};

//	Attribute FixMode class definition
class FixModeAttrib: public Tango::Attr
{
public:
	FixModeAttrib():Attr("FixMode",
			Tango::DEV_BOOLEAN, Tango::READ_WRITE) {};
	~FixModeAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_FixMode(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<VarianDualIP *>(dev))->write_FixMode(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_FixMode_allowed(ty);}
};

//	Attribute StartMode class definition
class StartModeAttrib: public Tango::Attr
{
public:
	StartModeAttrib():Attr("StartMode",
			Tango::DEV_BOOLEAN, Tango::READ_WRITE) {};
	~StartModeAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_StartMode(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<VarianDualIP *>(dev))->write_StartMode(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_StartMode_allowed(ty);}
};

//	Attribute Controller class definition
class ControllerAttrib: public Tango::Attr
{
public:
	ControllerAttrib():Attr("Controller",
			Tango::DEV_STRING, Tango::READ) {};
	~ControllerAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<VarianDualIP *>(dev))->read_Controller(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<VarianDualIP *>(dev))->is_Controller_allowed(ty);}
};


//=========================================
//	Define classes for commands
//=========================================
//	Command On class definition
class OnClass : public Tango::Command
{
public:
	OnClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	OnClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~OnClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_On_allowed(any);}
};

//	Command Off class definition
class OffClass : public Tango::Command
{
public:
	OffClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	OffClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~OffClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_Off_allowed(any);}
};

//	Command Reset class definition
class ResetClass : public Tango::Command
{
public:
	ResetClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ResetClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ResetClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_Reset_allowed(any);}
};

//	Command SetFixedMode class definition
class SetFixedModeClass : public Tango::Command
{
public:
	SetFixedModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SetFixedModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SetFixedModeClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_SetFixedMode_allowed(any);}
};

//	Command SetStartMode class definition
class SetStartModeClass : public Tango::Command
{
public:
	SetStartModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SetStartModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SetStartModeClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_SetStartMode_allowed(any);}
};

//	Command SetStepMode class definition
class SetStepModeClass : public Tango::Command
{
public:
	SetStepModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SetStepModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SetStepModeClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_SetStepMode_allowed(any);}
};

//	Command SetProtectMode class definition
class SetProtectModeClass : public Tango::Command
{
public:
	SetProtectModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SetProtectModeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SetProtectModeClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<VarianDualIP *>(dev))->is_SetProtectMode_allowed(any);}
};


/**
 *	The VarianDualIPClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  VarianDualIPClass : public Tango::DeviceClass
#else
class VarianDualIPClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(VarianDualIPClass::Additionnal DServer data members) ENABLED START -----*/

public:
	

	/*----- PROTECTED REGION END -----*/	//	VarianDualIPClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static VarianDualIPClass *init(const char *);
		static VarianDualIPClass *instance();
		~VarianDualIPClass();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		VarianDualIPClass(string &);
		static VarianDualIPClass *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	VarianDualIP_H
